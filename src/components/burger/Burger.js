import React from 'react'

import classes from './Burger.css'
import BurgerIngrediens from './ingrediens/Ingredien'


const burger = (props) => {

    let transformedIngrediens = Object.keys(props.ingrediens).map(igKey => {
            return [...Array(props.ingrediens[igKey])].map((_, i) => {
               return <BurgerIngrediens key={igKey + i} type={igKey}/>
            })
        }).reduce((arr, el) => {
            return arr.concat(el)
    }, []);
    
    if (transformedIngrediens.length === 0) {
        transformedIngrediens = <p>Plase start adding ingrediens!</p>
    }

    return(
        <div className={classes.Burger}>
            <BurgerIngrediens type="bread-top"/>
            {transformedIngrediens}
            <BurgerIngrediens type="bread-bootom"/>
        </div>
    )
}

export default burger;