import React from 'react'

import classes from './BuildControlls.css'
import BuildControl from './buildControl/BuildControl'

const controls = [
    {label: 'Salad', type: 'salad'},
    {label: 'Bacon', type: 'bacon'},
    {label: 'Cheese', type: 'cheese'},
    {label: 'Meat', type: 'meat'}
]

const buildControlls = (props) => (

    <div className={classes.BuildControlls}>
        <p>Current Price: <strong>{props.price.toFixed(2)}</strong></p>
        {controls.map(ctr => (
            <BuildControl key={ctr.label}
                          label={ctr.label}
                          added={() => props.ingredientAdded(ctr.type)}
                        removed={() => props.removeIngrediens(ctr.type)}
                        disabled={props.disabled[ctr.type]}/>
        ))}
        <button className={classes.OrderButton}
                disabled={!props.purchasable}
                onClick={props.ordered}>ORDER NOW
        </button>
    </div>
);

export default buildControlls;