import React, {Component} from 'react'
import Aux from '../../../hoc/auxx/Auxx'
import Button from '../../ui/button/Button'

class OrderSummary extends Component {

    render() {

        const ingredientSummary = Object.keys(this.props.ingrediens).map(igKey => {
            return (
                <li key={igKey}>
                    <span style={{textTransform:'capitalize'}}>{igKey}</span>: {this.props.ingrediens[igKey]}
                </li> );
        });
        return(
            <Aux>
                <h3>Your Order</h3>
                <p>A delicious burger with the following ingrediens:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total price: {this.props.price.toFixed(2)}</strong></p>
                <p>Continue to checkout?</p>
                <Button btnType="Danger" clicked={this.props.purchaseCancelled}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinue}>CONTINUE</Button>
            </Aux>
        )
    }
}

export default OrderSummary;