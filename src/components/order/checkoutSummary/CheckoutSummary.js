import React from 'react'

import Burger from '../../burger/Burger'
import Button from '../../ui/button/Button'
import classes from './CheckoutSummary.css'

const checkoutSummary = (props) => {
    return(
        <div className={classes.CheckoutSummary}>
            <h1>We hope it tastes well!</h1>
            <div style={{width: '100%', margin: 'auto'}}>
                <Burger ingrediens={props.ingrediens}/>
                <Button btnType="Danger" clicked={props.onCheckoutCanceled}>CANCEL</Button>
                <Button btnType="Success" clicked={props.checkoutContinued}>CONTINUE</Button>
            </div>
        </div>
    )
};

export default checkoutSummary;