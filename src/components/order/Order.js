import React from 'react'
import classes from './Order.css'

const Order = (props) => {
    const ingrediens = [];
    for (let ingredientName in props.ingrediens) {
        ingrediens.push({
            name: ingredientName,
            amount: props.ingrediens[ingredientName]
        })
    }

    const ingredientOutput = ingrediens.map(ig => {
        return <span
            style={{
                textTransform: 'capitalize',
                display: 'inline-block',
                margin: '0 8px',
                border: '1px solid #ccc',
                padding: '5px'
            }}
            key={ig.name}>{ig.name} ({ig.amount})</span>
    })
    return(
        <div className={classes.Order}>
            <p>Ingrediens: {ingredientOutput}</p>
            <p>Price: <strong>USD {Number.parseFloat(props.price).toFixed(2)}</strong></p>
        </div>
    )

};

export default Order;