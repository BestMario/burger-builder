import React from 'react'

import Logo from '../../logo/Logo'
import NavigationItems from '../navigationItems/NavigationItems'
import classes from './SideDrawer.css'
import Backdrop from '../../ui/backDrop/BackDrop'
import Aux from '../../../hoc/auxx/Auxx'

const SideDrawer = (props) => {

    let attachClasses = [classes.SideDrawer, classes.Close]
    if (props.open) {
       attachClasses = [classes.SideDrawer, classes.Open]
    }

    return (

        <Aux>
            <Backdrop show={props.open} clicked={props.closed}/>
            <div className={attachClasses.join(' ')}>
                <div className={classes.Logo}><Logo/></div>
                <nav>
                    <NavigationItems/>
                </nav>
            </div>
        </Aux>
    );
};

export default SideDrawer;