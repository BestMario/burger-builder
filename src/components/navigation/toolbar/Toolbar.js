import React from 'react'

import classes from './Toolbar.css'
import Logo from '../../logo/Logo'
import NavigationItems from '../navigationItems/NavigationItems'
import DrowToggle from '../sideDrawer/drawerToggle/DrawerToggle'

const toolbar = (props) => (
    <header className={classes.Toolbar}>
        <DrowToggle clicked={props.drowerToggleClicked}/>
        <div className={classes.Logo}><Logo/></div>
        <nav className={classes.DesktopOnly}>
              <NavigationItems/>
        </nav>
    </header>
);

export default toolbar;