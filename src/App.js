import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';
import './App.css';
import BurgerBuilder from "./containers/burgerBuilder/BurgerBuilder";
import Layout from './hoc/layout/Layout'
import Checkout from './containers/checkout/Checkout'
import Orders from "./containers/orders/Orders";

function App() {
    return (
        <BrowserRouter>
            <div>
                <Layout>
                    <Switch>
                        <Route path="/checkout" component={Checkout}/>
                        <Route path="/orders" component={Orders}/>
                        <Route path="/" component={BurgerBuilder}/>
                    </Switch>
                </Layout>
            </div>
        </BrowserRouter>
    );
}

export default App;
