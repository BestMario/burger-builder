import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://burger-builder-bdf58.firebaseio.com/'
});

export default instance;