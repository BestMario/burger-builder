import React, {Component} from 'react'

import Button from '../../../components/ui/button/Button'
import classes from './ContactData.css'
import axios from '../../../axios-orders'
import Spinner from '../../../components/ui/spiner/Spiner';


class ContactData extends Component {

    state = {
        name: '',
        email: '',
        address: {
            street: '',
            postalCode: ''
        },
        loading: false
    };

    orderHandler = (event) => {
        event.preventDefault();
        this.setState({loading: true})
        const order = {
            ingrediens: this.props.ingrediens,
            price: this.props.price,
            customer: {
                name: 'Mario',
                address: {
                    street: 'testStreet 1',
                    zipCode: '12345',
                    country: 'Poland'
                },
                email: 'mario@test.com'
            },
            deliveryMethod: 'fastest'

        };
        axios.post('/orders.json', order)
            .then(response => {
                this.setState({loading: false})
                this.props.history.push('/')
            })
            .catch(error => {
                this.setState({loading: false})
            })
    }

    render() {
        let form = (
            <form>
                <input className={classes.Input} type="text" name="mame" placeholder="Your Name"/>
                <input className={classes.Input} type="text" name="email" placeholder="Your Mail"/>
                <input className={classes.Input} type="text" name="street" placeholder="Street"/>
                <input className={classes.Input} type="text" name="postal" placeholder="Postal Code"/>
                <Button btnType="Success" clicked={this.orderHandler}>ORDER</Button>
            </form>);
        if (this.state.loading) {
            form = <Spinner/>
        }
        return (
            <div className={classes.ContactData}>
                <h4>Enter your contact data</h4>
                {form}
            </div>
        )
    }
}

export default ContactData;