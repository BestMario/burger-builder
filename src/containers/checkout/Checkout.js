import React, {Component} from 'react'
import {Route} from 'react-router-dom';
import CheckoutSummary from '../../components/order/checkoutSummary/CheckoutSummary'
import ContactData from './contactData/ContactData'


class Checkout extends Component {

    state = {
        ingrediens: null,
        price: 0
    };

    checkoutContinuedHandler = () => {
        this.props.history.replace('/checkout/contact-data')
    };

    checoutCanceledHandler = () => {
        this.props.history.goBack();
    };

    componentWillMount() {
        const query = new URLSearchParams(this.props.location.search);
        const ingrediens = {};
        let price = 0;
        for (let param of query.entries()) {
            if (param[0] === 'price') {
                price = param[1]
            } else {
                ingrediens[param[0]] = +param[1]
            }
        }
        this.setState({ingrediens: ingrediens, totalPrice: price})
    }

    render() {
        return(
            <div>
                <CheckoutSummary
                    ingrediens={this.state.ingrediens}
                    onCheckoutCanceled={this.checoutCanceledHandler}
                    checkoutContinued={this.checkoutContinuedHandler}/>
                    <Route path={this.props.match.path + '/contact-data'}
                           render={() => (
                               <ContactData
                                   ingrediens={this.state.ingrediens}
                                   price={this.state.totalPrice}
                                   {...this.props}/>)}/>
            </div>
        )
    }
}

export default Checkout;