import React, {Component} from 'react';
import Aux from '../../hoc/auxx/Auxx'
import Burger from '../../components/burger/Burger'
import BuildControlls from '../../components/burger/buildControlls/BuildControlls'
import Modal from '../../components/ui/modal/Modal'
import OrderSummary from '../../components/burger/orderSummary/OrderSummary'
import Spiner from '../../components/ui/spiner/Spiner'
import WithErrorHandler from '../../hoc/withErrorHandler/WithErrorHandler'
import axios from '../../axios-orders'

const INGREDIENDS_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
};

class BurgerBuilder extends Component {

    state = {
        ingrediens: null,
        totalPrice: 4,
        purchasable: false,
        purchasing: false,
        loading: false,
        error: false
    };

    componentDidMount() {
        axios.get('https://burger-builder-bdf58.firebaseio.com/orders/-MAkUuLHuF3QmETh3zeW/ingrediens.json')
            .then(response => {
                this.setState({ingrediens: response.data})
            })
            .catch(error => {
                this.setState({error:true})
            })
    }

    updatePurchaseState(ingrediens) {

        const sum = Object.keys(ingrediens)
            .map(igKey => {
                return ingrediens[igKey]
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);
        this.setState({purchasable: sum > 0})
    }

    addIngrediensHandler = (type) => {
        const oldCount = this.state.ingrediens[type];
        const updatedCount = oldCount + 1;
        const updatedIngrediens = {
            ...this.state.ingrediens
        };
        updatedIngrediens[type] = updatedCount;
        const priceAdition = INGREDIENDS_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAdition;
        this.setState({totalPrice: newPrice, ingrediens: updatedIngrediens})
        this.updatePurchaseState(updatedIngrediens)
    };

    removeIngrediensHandler = (type) => {
        const oldCount = this.state.ingrediens[type];
        if (oldCount <= 0) {
            return;
        }
        const updatedCount = oldCount - 1;
        const updatedIngrediens = {
            ...this.state.ingrediens
        };
        updatedIngrediens[type] = updatedCount;
        const priceDeduction = INGREDIENDS_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceDeduction;
        this.setState({totalPrice: newPrice, ingrediens: updatedIngrediens})
        this.updatePurchaseState(updatedIngrediens);
    };

    purchaseHandler = () => {
        this.setState({purchasing: true})
    };

    purchaseCancelHandler = () => {
        this.setState({purchasing: false})
    };

    purchaseContinueHandler = () => {

        const queryParams = [];
        for (let i in this.state.ingrediens) {
            queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingrediens[i]))
        }
        queryParams.push('price=' + this.state.totalPrice)
        const queryString = queryParams.join('&')
        this.props.history.push({
            pathname: 'checkout',
            search: '?' + queryString
        })
    };

    render() {
        const disabledInfo = {
            ...this.state.ingrediens
        };

        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        let orderSummary = null;
        let burger = this.state.error ? <p>Ingrediens cannot be loaded!</p> : <Spiner/>

        if (this.state.ingrediens) {
            burger = (
                <Aux>
                    <Burger ingrediens={this.state.ingrediens}/>
                    <BuildControlls
                        ingredientAdded={this.addIngrediensHandler}
                        removeIngrediens={this.removeIngrediensHandler}
                        disabled={disabledInfo}
                        purchasable={this.state.purchasable}
                        ordered={this.purchaseHandler}
                        price={this.state.totalPrice}/>
                </Aux>)
            orderSummary = <OrderSummary ingrediens={this.state.ingrediens}
                                         price={this.state.totalPrice}
                                         purchaseCancelled={this.purchaseCancelHandler}
                                         purchaseContinue={this.purchaseContinueHandler}/>
        }

        if (this.state.loading) {
            orderSummary = <Spiner/>
        }

        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        )
    }
}

export default WithErrorHandler(BurgerBuilder, axios);